from django.shortcuts import render
from .models import Shoe, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["bin_number", "import_href"]

# create shoe detail encoder
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "color",
        "manufacturer",
        "id",
    ]
    encoders = {
        "bin":BinVOEncoder()
    }
# create shoe List Encoder
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "manufacturer", "color", "id"]

    def get_extra_data(self, o):
        return {"bin": o.bin.bin_number}

# show list
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    """
    List shoes and link the shoe to the bin ID

    Returns dictionary with key "shoes", which is a list of
    shoe names and urls.
    """

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            bin_id = content["bin"]
            bin_vo = BinVO.objects.get(id=bin_id)
            content["bin"] = bin_vo
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )



# show detail
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
        except Shoe.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid Shoe ID"},
                    status=400
                )
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe Does not Exist"})
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=id)

            props = ["name", "color", "manufacturer"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe Does not Exist"})
            response.status_code = 404
            return response
