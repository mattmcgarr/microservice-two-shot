from django.db import models

# Create your models here.

# need to create a bin VO
class BinVO(models.Model):
    bin_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

class Shoe(models.Model):
    # need to track manufacturer, model name, color, url for a picture, bin in wordrobe where it exists
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)

    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE
    )
