import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const ShoeList = () => {
  const [shoes, setShoes] = useState([]);

  const getData = async () => {
    const resp = await fetch("http://localhost:8080/api/shoes/");
    if (resp.ok) {
      const data = await resp.json();
      setShoes(data.shoes);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const url = `http://localhost:8080/api/shoes/${e.target.id}`;
    const fetchCongig = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(url, fetchCongig);

    const data = await resp.json();

    setShoes(shoes.filter((shoe) => String(shoe.id) !== e.target.id));
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Shoe ID</th>
          <th>Shoe Name</th>
          <th>Manufacturer</th>
          <th>Color</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {shoes.map((shoe) => {
          return (
            <tr key={shoe.id}>
              <td>{shoe.id}</td>
              <td>
                <Link to={`/shoes/${shoe.id}`}>{shoe.name}</Link>
              </td>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.color}</td>
              <td>
                <button
                  onClick={handleDelete}
                  id={shoe.id}
                  type="button"
                  className="btn btn-danger"
                >
                  Delete Shoe
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default ShoeList;
