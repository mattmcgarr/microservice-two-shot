import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoeCreateForm from "./ShoeCreateForm";
import ShoeList from "./ShoeList";
import ShoeDetail from "./ShoeDetail";

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoeList shoes={props.shoes} />} />
            <Route path=":id" element={<ShoeDetail />} />
            <Route path="new" element={<ShoeCreateForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
