import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";

const ShoeDeatil = () => {
  const [shoe, setShoe] = useState({});
  const { id } = useParams();

  const getData = async () => {
    const resp = await fetch(`http://localhost:8080/api/shoes/${id}`);
    if (resp.ok) {
      const data = await resp.json();
      console.log(data);
      setShoe(data);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async () => {
    const url = `http://localhost:8080/api/shoes/${id}`;

    const fetchConfig = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const resp = await fetch(url, fetchConfig);
    const data = await resp.json();
  };

  return (
    <>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Shoe Details</h1>
            <h4>
              Name: <span>{shoe.name}</span>
            </h4>
            <h4>
              Manufacturer: <span>{shoe.manufacturer}</span>
            </h4>
            <h4>
              Color: <span>{shoe.color}</span>
            </h4>

            <Link to="/shoes" className="btn btn-primary">
              Return to Shoe List
            </Link>
            <button onClick={handleDelete} className="btn btn-danger">
              Delete Shoe
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ShoeDeatil;
